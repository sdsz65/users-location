import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query'
import './App.css';
import 'antd/dist/antd.css';
import Location from 'components/Location';

const queryClient = new QueryClient()
function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <Location />
      </div>
    </QueryClientProvider>
  );
}

export default App;

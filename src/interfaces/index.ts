export interface IActions {
    ADD_LOCATION: string;
    RESET_ERROR_MESSAGES_LOCATION: string;
}

export interface FileData { uid: any; url: any; name: any; type: string; size: number };

export interface ILocation {
    name: string;
    type: "Business" | "Home";
    logo?: FileData | null;
    coords: [number, number];
};

export interface IAddLocation {
    type: IActions['ADD_LOCATION'],
    payload: ILocation
}

export interface IResetErrorsLocation {
    type: IActions['RESET_ERROR_MESSAGES_LOCATION']
}

export type Props = {
    [key: string]: any;
};


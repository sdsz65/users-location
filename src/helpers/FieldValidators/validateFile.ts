import { FileData } from 'interfaces/index';
import { i18n } from 'i18n/en_US';

export const validateFile = (value: FileData | undefined | null, extensions: string, size: number) => {
    let errors;
    if (value && (value.type || value.type === '') && value.size && extensions) {
        const typeInfo = value.type.split('/');
        const extensionArray = extensions.split(',').map(item => item.replace('.', ''));
        
        if (!extensionArray.includes(typeInfo[1])) {
            errors = i18n.FILE_EXTENSION_ERROR(extensionArray.toString());
        } else if (value.size > (size * 1024 * 1024)) {
            errors = i18n.FILE_SIZE_ERROR(size);
        }
    }
    
    return errors;
};

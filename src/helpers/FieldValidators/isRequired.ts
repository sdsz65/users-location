import { i18n } from 'i18n/en_US';

export const isRequired = (value: string | number) => {
  if (Array.isArray(value)) {
    return !value.length ? i18n.FORM_IS_REQUIRED_FIELD : '';
  }
  return !value && value !== 0 ? i18n.FORM_IS_REQUIRED_FIELD : '';
};

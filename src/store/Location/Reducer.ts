import { LOCATION_NEW, LOCAION_RESET_ERROR_MESSAGE } from './ActionTypes';
const initialState: any = {
    locations: [],
    createLocationHasError: null
};

export default function adminUserReducer(state = initialState, action: {type: string, payload?: any}) {
    switch (action.type) {
        case LOCATION_NEW: 
            return {
                ...state,
                locations: [...state.locations, action.payload],
                createLocationHasError: false
            };

        case LOCAION_RESET_ERROR_MESSAGE: 
            return {
                ...state,
                createLocationHasError: null
            };
        default:
            return {...state};
    };
};
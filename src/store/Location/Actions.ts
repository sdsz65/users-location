import { LOCATION_NEW, LOCAION_RESET_ERROR_MESSAGE } from './ActionTypes';
import { IAddLocation, ILocation, IResetErrorsLocation } from 'interfaces';
type DispatchAddLocation = (arg: IAddLocation) => (IAddLocation);

export const createLocationAction = (data: ILocation) => async (dispatch: DispatchAddLocation) => {
    dispatch({
      type: LOCATION_NEW,
      payload: data
    });
};

type DispatchResetErrorsLocation = (arg: IResetErrorsLocation) => (IResetErrorsLocation);
export const resetErrorMessageAction = () => (dispatch: DispatchResetErrorsLocation) => {
  dispatch({
    type: LOCAION_RESET_ERROR_MESSAGE
  });
};
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { rootReducer } from './rootReducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: [],
};
  
const persistedReducer = persistReducer(persistConfig, rootReducer);

let composeEnhancers = compose;

export default function configureStore(initialState = {}) {
    const store = createStore(
      persistedReducer,
      initialState,
      composeEnhancers(applyMiddleware(thunk))
    );
  
    return { store, persistor: persistStore(store) };
}
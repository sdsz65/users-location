import { combineReducers } from 'redux';
import locationReducer from './Location/Reducer';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

const locationPersistConfig = {
    key: 'location',
    storage,
    whitelist: ['locations'],
};



export const rootReducer = combineReducers({
    location: persistReducer(locationPersistConfig, locationReducer)
});

export type RootState = ReturnType<typeof rootReducer>
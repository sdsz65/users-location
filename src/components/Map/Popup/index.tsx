import React, {FC} from 'react';
import { Popup } from 'react-leaflet';
import { ILocation } from 'interfaces';

const UserDetails: FC<{details: ILocation}> = (props): JSX.Element => {
    return (
        <Popup>
            <div>
                <h1>Location Details</h1>
                <div>Location Name: {props.details.name} </div>
                <div>Location Type: {props.details.type} </div>
            </div>
        </Popup>
    );
};

export default UserDetails;
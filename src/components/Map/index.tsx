import React from 'react';
import { useSelector } from 'react-redux';
import UserDetailsPopup from './Popup';
import L from 'leaflet';
import { MapContainer, TileLayer, Marker } from 'react-leaflet'
import {RootState} from 'store/rootReducer';
import 'leaflet/dist/leaflet.css';
import { mapCenter } from 'config';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;

const Map = () => {
    const locations = useSelector( (state: RootState) => state.location.locations);

    return (
        <>
            <MapContainer center={mapCenter} zoom={13}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {
                    locations.map((item: any, key: number) => (
                        <Marker key={'marker-'+key} position={[item.coords[0], item.coords[1]]}>
                            <UserDetailsPopup details={{
                                name: item.name,
                                type: "Business",
                                coords: [item.coords[0], item.coords[1]]
                            }} />
                        </Marker>
                    ))
                }
            </MapContainer>
        </>
    );  
};

export default Map;
import React, { useState } from 'react';
import { i18n } from 'i18n/en_US';
import { Button, Modal } from 'antd';
import AddLocationForm from '../New/Form';

const AddLocation = () => {
    const [visible, setVisible] = useState(false);
    const showModal = () => {
        setVisible(true);
    };
    const hideModal = () => {
        setVisible(false);
    };

    return (
        <>
            <Modal
                destroyOnClose
                title={i18n.ADD_LOCATION}
                visible={visible}
                footer={null}
                style={{ top: 20 }}
                onCancel={hideModal}
            >
                <AddLocationForm hideModal={hideModal} />
            </Modal>
            <Button 
                style={{float: 'right', marginTop: 12, marginRight: '10%'}}
                onClick={showModal}    
            >
                {i18n.ADD_LOCATION}
            </Button>
        </>
    );
};

export default AddLocation;
import React from 'react';
import { Field, Form } from 'formik';
import { Button, Col, Row } from 'antd';
import { FormikProps } from "formik";
import {
    AntInput,
    AntSelect,
    AntMap,
    AntUpload
} from 'components/UI/FormItems';
import { isRequired, validateFile } from 'helpers/FieldValidators';
import { i18n } from 'i18n/en_US';
import { ILocation } from 'interfaces';

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
        md: { span: 24 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
        md: { span: 24 },
    },
};

const FormicForm = (props: { hideModal: any } & FormikProps<ILocation>) => {
    const {
        handleSubmit,
        submitCount,
        isSubmitting,
        hideModal,
        setFieldValue,
        values
    } = props;

    return (
        <Form onSubmit={handleSubmit}>
            <fieldset>
                <Field
                    label={i18n.LOCATION_NAME}
                    component={AntInput}
                    name="name"
                    isrequired={'true'}
                    validate={isRequired}
                    type="text"
                    submitCount={submitCount}
                    hasFeedback
                    formItemLayout={formItemLayout}
                />
                <Field
                    label={i18n.LOCATION_ON_MAP}
                    component={AntMap}
                    name="coords"
                    submitCount={submitCount}
                    hasFeedback
                    formItemLayout={formItemLayout}
                    setFieldValue={setFieldValue}
                />
                <Field
                    label={i18n.LOCATION_TYPE}
                    component={AntSelect}
                    name="type"
                    submitCount={submitCount}
                    hasFeedback
                    formItemLayout={formItemLayout}
                    selectOptions={['Business', 'Home']}
                />
                <Field
                    label={i18n.LOGO}
                    buttonLabel={i18n.CLICK_OR_DRAG_FILE_HERE}
                    component={AntUpload}
                    setFieldValue={setFieldValue}
                    name="logo"
                    key="logo"
                    formItemLayout={formItemLayout}
                    accept='.png,.jpg,.jpeg,.gif'
                    hasFeedback={true}
                    validate={validateFile(values.logo, '.png,.jpg,.jpeg,.gif', 20)}
                    submitCount={submitCount}
                />
            </fieldset>
            <Row justify="end" align="middle">
                <Col>
                    <Button onClick={hideModal} disabled={isSubmitting} style={{marginRight: 8}}>
                        {i18n.CANCEL}
                    </Button>
                </Col>
                <Col>
                    <Button
                        htmlType="submit"
                        type="primary"
                        loading={isSubmitting}
                        disabled={isSubmitting}
                    >
                        {i18n.ADD}
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default FormicForm;
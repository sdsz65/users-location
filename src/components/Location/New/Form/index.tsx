import React, { useEffect, useCallback } from 'react';
import { FormikHelpers, Formik } from "formik";
import { useSelector, useDispatch } from 'react-redux';
import { createLocationAction, resetErrorMessageAction } from 'store/Location/Actions';
import FormRender from '../FormRender';
import { Message } from 'components/UI/Message';
import { i18n } from 'i18n/en_US';
import { RootState } from 'store/rootReducer';
import { Props, ILocation } from 'interfaces';
import { mapCenter } from 'config';

const initialValues : ILocation = {
    name: '',
    type: 'Business',
    logo: null,
    coords: mapCenter
};

const Form = (props: Props) => {
    const { hideModal } = props;
    const dispatch = useDispatch();
    const createLocationHasError = useSelector((state: RootState) => state.location.createLocationHasError);

    const handleSubmit = ({ name, type, logo, coords }: ILocation, formikHelpers: FormikHelpers<ILocation>) => {
        formikHelpers.setSubmitting(true);
        dispatch(
            createLocationAction({
                name: name,
                type: type,
                logo: logo,
                coords: coords
            })
        );
    };

    const handleResetResult = useCallback((cb) => {
        dispatch(resetErrorMessageAction());
        cb && cb();
    }, [dispatch]);

    const handleSubmitResult = useCallback(() => {
        if (createLocationHasError === true) {
            Message({
                type: 'error',
                text: i18n.ADD_LOCATION_ERROR,
            });
        } else if (createLocationHasError === false) {
            Message({
                type: 'success',
                text: i18n.ADD_LOCATION_SUCCESS,
            });
            handleResetResult(() => {
                hideModal();
            });
        }
    }, [
        handleResetResult,
        createLocationHasError,
        hideModal
    ]);

    useEffect(() => {
        handleSubmitResult();
    }, [handleSubmitResult]);

    return (
        <Formik
          enableReinitialize
          initialValues={initialValues}
          onSubmit={handleSubmit}
          render={(formikProps) => (
            <FormRender {...formikProps} hideModal={hideModal} />
          )}
        />
    );
};

export default Form
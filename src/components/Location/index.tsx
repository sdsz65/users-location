import React from 'react';
import Map from 'components/Map';
import AddLocation from './AddLocation';

const Location = () => {
    return (
        <>
            <Map />
            <AddLocation />
        </>
    )
};

export default Location;
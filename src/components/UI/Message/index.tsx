import { message } from 'antd';

const msgVariable = {
  type: {
    success: 'success',
    error: 'error',
    warning: 'warning',
    loading: 'loading',
  },
  text: '',
  duration: 3,
};

export function Message({
  type = '',
  text = msgVariable.text,
  duration = msgVariable.duration,
}) {
  switch (type) {
    case msgVariable.type.success:
      message.success(text, duration);
      break;
    case msgVariable.type.error:
      message.error(text, duration);
      break;
    case msgVariable.type.warning:
      message.warning(text, duration);
      break;
    case msgVariable.type.loading:
      message.loading(text, duration);
      break;
    default:
      message.info(text, duration);
      break;
  }
}

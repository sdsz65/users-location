import React from 'react';
import { Form, Select, Col, Row } from 'antd';
import InputLabel from '../InputLabel';
const FormItem = Form.Item;
const { Option } = Select;

type FieldProps = {
  field: {name: string, value: any};
  form: {errors: any, touched: any, setFieldValue: any, setFieldTouched: any};
  hasFeedback: boolean;
  label: string | undefined | null;
  selectOptions: Array<string> | undefined | null;
  submitCount: number;
  type: string;
  formItemLayout: any;
  onChangeValue: any;
  isrequired: boolean | string | undefined;
};
const CreateAntField = (AntComponent: any, fieldType='text') => ( props: FieldProps) => {
  const touched = props.form.touched[props.field.name];
  const submitted = props.submitCount > 0;
  const hasError = props.form.errors[props.field.name];
  const submittedError = hasError && submitted;
  const touchedError = hasError && touched;

  const onChange = (e: any) => {
    const value = e?.target?.value || e;
    if (props.form?.setFieldValue) {
      props.form.setFieldValue(props.field.name, value);
    }
    if (props.onChangeValue) {
      props.onChangeValue(props.field.name, e.target.value);
    }
  };

  const onBlur = () => props.form.setFieldTouched(props.field.name, true);

  const handleKeyPress = (event: any) => {
    if (event.key === '.') {
      event.preventDefault();
    }
  };

  return (
    <div className='field-container'>
      <FormItem
        {...props.formItemLayout}
        label={
          <InputLabel
            label={props.label ? props.label : null}
            required={props.isrequired === 'true'}
          />
        }
        help={submittedError || touchedError ? hasError : null}
        validateStatus={submittedError || touchedError ? 'error' : 'success'}
        className='ltrInput'
      >
        <Row>
          <Col flex="auto">
            <AntComponent
              autoComplete="new-password"
              {...props.field}
              {...props}
              value={
                ((!props.selectOptions && fieldType !== 'select') ||
                  (props.selectOptions && props.selectOptions.filter((item: any) =>
                  (item.key || item) === (props.field.value?.value || props.field.value)
                ).length) ?
                props.field.value : null)
              }
              type={props.type === 'integer' ? 'number' : props.type}
              onKeyDown={props.type === 'integer' ? handleKeyPress : undefined}
              onBlur={onBlur}
              onChange={onChange}
            >
              {
                props.selectOptions &&
                props.selectOptions.map((optionItem: any) => (
                  <Option
                    className="optionClassName"
                    key={optionItem?.key || optionItem}
                    value={optionItem?.key || optionItem}
                  >
                    {optionItem?.label || optionItem}
                  </Option>
                ))}
            </AntComponent>
          </Col>
        </Row>
      </FormItem>
    </div>
  );
};

export default CreateAntField;
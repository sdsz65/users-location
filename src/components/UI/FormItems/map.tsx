import React, { useState, useRef, useMemo } from 'react';
import { Form, Col, Row } from 'antd';
import { MapContainer, TileLayer, Marker } from 'react-leaflet'
import InputLabel from '../InputLabel';
import { mapCenter } from 'config';
import './index.css';
const FormItem = Form.Item;

type FieldProps = {
    field: { name: string, value: any };
    form: { errors: any, touched: any, setFieldValue: any, setFieldTouched: any };
    hasFeedback: boolean;
    label: string | undefined | null;
    submitCount: number;
    formItemLayout: any;
};

const CreateMap = (props: FieldProps) => {
    const touched = props.form.touched[props.field.name];
    const submitted = props.submitCount > 0;
    const hasError = props.form.errors[props.field.name];
    const submittedError = hasError && submitted;
    const touchedError = hasError && touched;
    const [position, setPosition] = useState(mapCenter)
    const markerRef = useRef(null)

    const eventHandlers = useMemo(
        () => ({
            dragend() {
                const marker: any = markerRef.current;
                if (marker != null) {
                    const location = marker.getLatLng();
                    setPosition(location)
                    props.form.setFieldValue(props.field.name, [location.lat, location.lng]);
                }
            },
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    , [])

    return (
        <div className='field-container'>
            <FormItem
                {...props.formItemLayout}
                label={<InputLabel label={props.label ? props.label : null}/>}
                validateStatus={submittedError || touchedError ? 'error' : 'success'}
                className={'ltrInput'}
            >
                <Row>
                    <Col flex="auto">
                        <MapContainer
                            center={mapCenter}
                            zoom={13}
                        >
                            <TileLayer
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            />
                            <Marker
                                draggable={true}
                                eventHandlers={eventHandlers}
                                position={position}
                                ref={markerRef}
                            />
                        </MapContainer>
                    </Col>
                </Row>
            </FormItem>
        </div>
    );
};

export default CreateMap;
import { Input, Select } from 'antd';
import CreateAntField from './field';
import CreateMap from './map';
import CreateUpload from './upload';

export const AntSelect = CreateAntField(Select, 'select');
export const AntInput = CreateAntField(Input);
export const AntMap = CreateMap;
export const AntUpload = CreateUpload;
  
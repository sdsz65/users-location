import React, { useEffect, useState } from 'react';
import { Form, Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import InputLabel from '../InputLabel';
import { FileData } from 'interfaces';

const FormItem = Form.Item;
type FieldProps = {
    field: {name: string, value: any};
    form: {errors: any, touched: any, setTouched: any, setFieldValue: any, setFieldTouched: any};
    hasFeedback: boolean;
    label: string | undefined | null;
    help: string  | undefined;
    submitCount: number;
    formItemLayout: any;
    size: number;
    accept: string;
    extensions: any;
};

const CreateUpload = (props: FieldProps) => {
    const touched = props.form.touched[props.field.name];
    const submitted = props.submitCount > 0;
    const hasError = props.form.errors[props.field.name];
    const submittedError = hasError && submitted;
    const touchedError = hasError && touched;

    const [fileList, setFileList] = useState<FileData[]>([]);
    //{ uid: any; url: any; name: any; }[]
    useEffect(() => {
        let initialFileList = [];
        if (props.field?.value && props.field.value.url && props.field.value.url.length) {
            initialFileList = [
                {
                    uid: props.field.value.uid,
                    url: props.field.value.url,
                    name: props.field.value.name,
                    type: props.field.value.type,
                    size: props.field.value.size,
                },
            ];
            setFileList(initialFileList);
        }
        if (props.field?.value) {
            // const error = validateFile(field.value, extensions, size);

            props.form.setTouched({[props.field.name]: true, ...props.form.touched});
            // form.setErrors({[field.name]: error, ...form.errors});
        }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.field.value]);

    const propsUpload = {
        multiple: false,
        fileList,
        name: props.field.name,
        showDownloadIcon: false,
        accept: props.accept,
        onChange(info: any) {
            if (info.fileList && info.fileList.length) {
                setFileList(
                    info.fileList.filter((item: any) => item.uid === info.file.uid)
                );
                if (props.form?.setFieldValue) {
                    props.form.setFieldValue(props.field.name, info.file.originFileObj);
                }
            } else {

                setFileList([]);
                if (props.form.setFieldValue) {
                    props.form.setFieldValue(props.field.name, {});
                }

            }
        },
        onBlur() { props.form.setFieldTouched(props.field.name, true)},
        customRequest(params: { file: any; onSuccess?: any; }) {
            setTimeout(() => {
                params.onSuccess('ok');
            }, 0);
        },
    };

    return (
        <div className='field-container'>
            <FormItem
                {...props.formItemLayout}
                label={
                    <InputLabel label={props.label || null}/>
                }
                help={submittedError || touchedError ? hasError : props.help}
                validateStatus={submittedError || touchedError ? 'error' : 'success'}
                className='ltrInput'
            >
                <Upload {...propsUpload} listType='picture'>
                    <UploadOutlined />
                </Upload>
            </FormItem>
        </div>
    );
};

export default CreateUpload;
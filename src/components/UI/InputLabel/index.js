import React from 'react';

const InputLabel = (props) => {
  return (
    <span>
      {props.label}
      {props.required ? (
        <span style={{ color: 'red' }}>&nbsp;*&nbsp;</span>
      ) : (
        ''
      )}
    </span>
  );
};

export default InputLabel;

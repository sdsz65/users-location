export const i18n = {
    ADD: 'Add',
    CANCEL: 'Cancel',
    NAME: 'Name',
    LOCATION_NAME: 'Location name',
    LOCATION_TYPE: 'Location Type',
    LOCATION_ON_MAP: 'Location on map',
    LOGO: 'Logo',
    CLICK_OR_DRAG_FILE_HERE: 'Click or drag file here',
    ADD_LOCATION: 'Add Location',
    ADD_LOCATION_ERROR: 'An error occurred in add location operation. Please try again later',
    ADD_LOCATION_SUCCESS: 'Location added successfully',
    FORM_IS_REQUIRED_FIELD: 'This Field Is Required',
    FILE_EXTENSION_ERROR : (extensions: string) => `Only ${extensions} files are allowed`,
    FILE_SIZE_ERROR: (size: number) => `The maximum supported file size is ${size} MB`
};
